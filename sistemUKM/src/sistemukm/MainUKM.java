package sistemukm;

public class MainUKM {

    public static void main(String[] args) {

        UKM ukm = new UKM();
        Mahasiswa ketua = new Mahasiswa();
        Mahasiswa sekretaris = new Mahasiswa();
        ketua.setNama("axel");
        sekretaris.setNama("getah");

        ukm.setNamaUnit("UKM TENNIS MEJA");
        ukm.setKetua(ketua);
        ukm.setSekretaris(sekretaris);

        Mahasiswa mahasiswa = new Mahasiswa();
        MasyarakatSekitar masyarakat = new MasyarakatSekitar();

        masyarakat.setNama("Edward");
        masyarakat.setNomor(12);
        masyarakat.setTempatTanggalLahir("Sidikalang 04-Maret-2000");

        mahasiswa.setNama("Frans");
        mahasiswa.setNim("195314167");
        mahasiswa.setTempatTanggalLahir("NYC 04-MARET-2001");

        System.out.println("Nama Unit   : " + ukm.getNamaUnit());
        System.out.println("Ketua       : " + ukm.getKetua().getNama());
        System.out.println("Sekretaris  : " + ukm.getSekretaris().getNama());

        System.out.println("Daftar Anggota UKM");
        System.out.println("-----------------------------------------------------");
        System.out.println("Nama    : " + masyarakat.getNama());
        System.out.println("nomor   : " + masyarakat.getNomor());
        System.out.println("Tempat Tanggal Lahir    : " + masyarakat.getTempatTanggalLahir());
        System.out.println("Iuran   :" + masyarakat.hitungIuran());
        System.out.println("-----------------------------------------------------");
        System.out.println("Nama    : " + mahasiswa.getNama());
        System.out.println("nomor   : " + mahasiswa.getNim());
        System.out.println("Tempat Tanggal Lahir    : " + mahasiswa.getTempatTanggalLahir());
        System.out.println("Iuran   :" + mahasiswa.hitungIuran());
        System.out.println("-----------------------------------------------------");
        double totalIuran = masyarakat.hitungIuran() + mahasiswa.hitungIuran();
        System.out.println("Total Iuran : " + totalIuran);

    }

}
