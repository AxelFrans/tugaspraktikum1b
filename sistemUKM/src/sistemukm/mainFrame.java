
package sistemukm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
//import tambahMahasiswa_dialog;
//import view.tambahMasyarakat_dialog;
//import view.tambahUKM_dialog;


public class mainFrame extends JFrame implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent ae) {

    }

    private JMenuBar menuBar;

    private JMenuItem menuItemEdit_tambahMhs;
    private JMenuItem menuItemEdit_tambahMasyarakat;
    private JMenuItem menuItemEdit_tambahUKM;

    private JMenuItem menuItemFile_lihatData;
    private JMenuItem menuItemFile_exit;

    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenu menu_Help;

    public mainFrame() {
        initComponents();
    }

    private void initComponents() {
        menuBar = new JMenuBar();

        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menu_Help = new JMenu("Help");

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        this.setJMenuBar(menuBar);

        menuItemEdit_tambahMhs = new JMenuItem("Tambah Mahasiswa");
        menuItemEdit_tambahMasyarakat = new JMenuItem("Tambah Masyarakat");
        menuItemEdit_tambahUKM = new JMenuItem("Isi Data UKM");
        menuItemFile_lihatData = new JMenuItem("Lihat Data");
        menuItemFile_exit = new JMenuItem("Exit");

        menu_Edit.add(menuItemEdit_tambahMhs);
        menu_Edit.add(menuItemEdit_tambahMasyarakat);
        menu_Edit.add(menuItemEdit_tambahUKM);

        menu_File.add(menuItemFile_lihatData);
        menu_File.add(menuItemFile_exit);
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);

        
        menuItemFile_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
            
    
            menuItemFile_lihatData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_TambahMahasiswa addMhs = new dialog_TambahMahasiswa();
                
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);

                
            }
        });


        menuItemEdit_tambahMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_TambahMahasiswa addMhs = new dialog_TambahMahasiswa();
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);
            }
        });


        menuItemEdit_tambahMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_TambahMahasiswa addMasyarakat = new dialog_TambahMahasiswa();
                addMasyarakat.setSize(400, 300);
                addMasyarakat.setVisible(true);
            }
        });


        menuItemEdit_tambahUKM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_DataUKM addUKM = new dialog_DataUKM();
                addUKM.setSize(400, 460);
                addUKM.setVisible(true);
            }
        });

    }


    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame main = new mainFrame();
                main.setSize(400, 500);
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }

}
