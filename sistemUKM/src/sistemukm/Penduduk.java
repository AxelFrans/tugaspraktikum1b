package sistemukm;

public abstract class Penduduk {

    private String nama;
    private String tempatTanggalLahir;

    public Penduduk() {

    }

    public Penduduk(String dataNama, String dataTempatTanggalLahir) {

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }

    public void setTempatTanggalLahir(String tempatTanggalLahir) {
        this.tempatTanggalLahir = tempatTanggalLahir;
    }

    public abstract double hitungIuran();

}
